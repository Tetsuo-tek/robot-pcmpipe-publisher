#!/bin/sh

 mplayer \
         -af resample=44100,channels=2,format=floatle \
         -ao pcm:nowaveheader:file=/dev/stdout \
         -quiet \
         -really-quiet \
         -playlist bassdrive.m3u  | ./RobotPcmPipePublisher.bin
