#ifndef ROBOTPCMPIPEPUBLISHER_H
#define ROBOTPCMPIPEPUBLISHER_H

#include <Core/System/Network/FlowNetwork/skflowsat.h>
#include <Core/System/Network/FlowNetwork/skflowaudiopublisher.h>
#include <Core/System/IPC/skstdinput.h>

/*
    GETTING DATA FROM STDOUT OF MPLAYER (FOR EXAMPLE)
    AND PUTTING THEM INSIDE SONUSD (THROUGHT /dev/stdin)
    BELOW THERE ARE EXAMPLE FOR OTHER DECODERs (ffmpeg and ffplay)

MAKE pl.txt
    $ find <DIR> -type f \
        \( -iname \*.ogg -o -iname \*.mp3 -o -iname \*.flac -o -iname \*.wav \) > pl.txt

    EXAMPLE INPUT
    $ mplayer \
        -af resample=48000,channels=2,format=<value> \
        -ao pcm:nowaveheader:file=/dev/stdout \
        -quiet \
        -really-quiet \
        -shuffle \
        -loop \
        -playlist pl.txt | RobotPcmPipePublisher ..

    WHERE format <value> for mplayer CAN BE:
        u8 s8 u16le s16le u32le s32le floatle
    AND RESPECTIVE fmt <value> strings for sonusd.json
        U8 S8 U16   S16   U32   S32   FLOAT

    A PIPE FROM ffplay with one generic input
    $ ffmpeg \
        -i http://www.xxx.ttt:8090/YYYY \
        -f f32le \
        -ar 48000 \
        -ac 2 pipe:1 | RobotPcmPipePublisher ..

    $ ffplay \
        -f concat \
        -safe 0 \
        -i pl.txt \
        -autoexit \
        -f f32le \
        -ar 48000 \
        -ac 2 pipe:1 | RobotPcmPipePublisher ..

*/

class RobotPcmPipePublisher extends SkFlowSat
{
    SkStdInput *input;
    SkRingBuffer inputProduction;
    SkFlowAudioPublisher *publisher;

    public:
        Constructor(RobotPcmPipePublisher, SkFlowSat);

    private:
        bool onSetup()                                  override;
        void onInit()                                   override;
        void onQuit()                                   override;

        void onFastTick()                               override;
};

#endif // ROBOTPCMPIPEPUBLISHER_H
