#include "robotpcmpipepublisher.h"
#include <Core/Containers/skarraycast.h>

ConstructorImpl(RobotPcmPipePublisher, SkFlowSat)
{
    input = nullptr;

    setObjectName("RobotPcmPipePublisher");
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool RobotPcmPipePublisher::onSetup()
{
    SkCli *cli = skApp->appCli();

    uint channels = cli->value("--channels").toUInt();
    uint sampleRate = cli->value("--sample-rate").toUInt();
    uint bufferFrames = cli->value("--buffer-frames").toUInt();
    SkString fmt = cli->value("--format").toString();

    SkAudioFmtType fmt_t;

    if (fmt == "CHAR")
        fmt_t = AFMT_CHAR;

    else if (fmt == "SHORT")
        fmt_t = AFMT_SHORT;

    else if (fmt == "INT")
        fmt_t = AFMT_INT;

    else if (fmt == "FLOAT")
        fmt_t = AFMT_FLOAT;

    publisher = new SkFlowAudioPublisher(this);
    publisher->setObjectName(this, "Publisher");

    setupPublisher(publisher);

    //WANTs INTERLEAVED
    SkAudioParameters p(false, channels, sampleRate, bufferFrames, fmt_t);

    if (!publisher->setup(p))
        return false;

    inputProduction.setObjectName(this, "Input");
    publisher->setInputBuffer(&inputProduction);
    publisher->enableFftGaussianFilter(cli->value("--fft-gauss-filter").toBool());

    return true;
}

void RobotPcmPipePublisher::onInit()
{
    publisher->start();

    input = new SkStdInput(this);
    input->setObjectName(this, "StdInput");
    input->open();

    ObjectMessage("Recording STARTED from STDIN stream ..");
}

void RobotPcmPipePublisher::onQuit()
{
    if (input)
    {
        input->disconnect();
        input->destroyLater();
        input = nullptr;
    }

    publisher->stop();
    ObjectMessage("Recording STOPPED");
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void RobotPcmPipePublisher::onFastTick()
{
    if (!input)
        return;

    while(input->bytesAvailable() > 0)
        input->read(&inputProduction);

    if (inputProduction.isCanonicalSizeReached())
        publisher->tick();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
