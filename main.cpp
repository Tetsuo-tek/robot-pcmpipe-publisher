#include "robotpcmpipepublisher.h"

SkApp *skApp = nullptr;

int main(int argc, char *argv[])
{
    skApp = new SkApp(argc, argv);

    SkCli *cli = skApp->appCli();

    cli->add("--buffer-frames",         "-b", "1024",       "Setup the frames count inside tick-buffer");
    cli->add("--channels",              "-c", "2",          "Setup channels");
    cli->add("--format",                "-f", "FLOAT",      "Setup the sample type (CHAR, SHORT, INT, FLOAT)");
    cli->add("--sample-rate",           "-r", "44100",      "Setup the capturing sample rate");
    cli->add("--fft-gauss-filter",      "-g", "",           "Enable gaussian window filter on fft calculations");

#if defined(ENABLE_HTTP)
    SkFlowSat::addHttpCLI();
#endif

    skApp->init(3000, 150000, SK_TIMEDLOOP_RT);
    new RobotPcmPipePublisher;

    return skApp->exec();
}
